const express = require('express')
const logger = require('morgan')
const errorhandler = require('errorhandler')
const mongodb = require('mongodb')
const bodyParser = require('body-parser')

const url = 'mongodb://localhost:27017/producto'
let app = express()
app.use(logger('dev'))
app.use(bodyParser.json())


mongodb.MongoClient.connect(url, { useUnifiedTopology: true }, (error, client) => {
    if (error) return process.exit(1)

    var db = client.db('producto')

    app.get('/producto', (req, res) => {//GET
        db.collection('productos')
            .find({}, { sort: { _id: -1 } })
            .toArray((error, productos) => {
                if (error) return next(error)
                res.send(productos)
            })
    });

    app.post('/producto', (req, res) => {//POST
        let newProducto = req.body
        db.collection('productos').insertOne(newProducto, (error, results) => {
            if (error) return next(error)
            res.send(results)
        })
    });

    app.put('/producto/:id', (req, res) => {//PUT
        db.collection('productos')
            .updateOne({ _id: mongodb.ObjectID(req.params.id) },
                { $set: req.body },
                (error, results) => {
                    if (error) return next(error)
                    res.send(results)
                }
            )
    });

    app.delete('/producto/:id', (req, res) => {//DELETE

        db.collection('productos').deleteOne({ _id: mongodb.ObjectID(req.params.id) },
            { $set: req.body },
            (error, results) => {
                if (error) return next(error)
                res.send(results)
            })
    });

});

app.listen(3000, () => console.log("Funciona"));