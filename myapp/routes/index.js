var express = require('express');
const Producto = require('../models/Producto');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  Producto.find({},function(error,data){
    res.render('index', {
      Producto: data
    });
  });

});

module.exports = router;
