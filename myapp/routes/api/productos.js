
let express = require('express');

let router = express.Router();

let productoControllerAPI = require("../../controllers/api/productoControllerAPI");



router.get("/", productoControllerAPI.producto_list);

router.post("/create", productoControllerAPI.producto_create);

router.delete("/:id/delete", productoControllerAPI.producto_delete);

router.put("/:id/update", productoControllerAPI.producto_update);

module.exports = router;

