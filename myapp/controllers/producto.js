let Produc = require("../models/Producto");

exports.producto_list = function (req, res) {

    Produc.find({}, function (error, data) {
        res.render("productos/index", { prods: data });
    });

}

exports.producto_create_get = function (req, res) {

    res.render("productos/create");

}


exports.producto_create_post = function (req, res) {

    Produc.create({
        id: req.body.id,
        nombre: req.body.nombre,
        precio: req.body.precio,
        descripcion: req.body.descripcion
    }, function (error) {
        res.redirect("/productos");
    });

}


exports.producto_delete_post = function (req, res) {

    Produc.deleteOne({ id: req.body.id }, function (error) {
        res.redirect("/productos");
    });

}


exports.producto_update_get = function (req, res) {

    Produc.find({ id: req.params.id }, function (error, data) {
        console.log(data);
        res.render("productos/update", data[0])
    })


}

exports.producto_update_post = function (req, res) {
    
    Produc.update({ id: req.params.id }, {
        id: req.body.id,
        nombre: req.body.nombre,
        precio: req.body.precio,
        descripcion: req.body.descripcion
    },function (error) {
        res.redirect("/productos");
    });

}