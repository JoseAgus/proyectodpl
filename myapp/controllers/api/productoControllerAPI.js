
let Prods = require("../../models/Producto");


exports.producto_list = function (req, res) {

    Prods.find({}, function (error, data) {
        if (error) res.status(500).send(error.message);


        res.status(200).json({

            producto: data

        });
    });

};

exports.producto_create = function (req, res) {

    Prods.create({
        id: req.body.id,
        nombre: req.body.nombre,
        precio: req.body.precio,
        descripcion: req.body.descripcion
    }, function (error,Newpro) {
        if (error) res.status(500).send(error.message);
        res.status(201).json(Newpro);
    });

};


exports.producto_delete = function (req, res) {

    Prods.findByIdAndDelete(req.params.id , function (error, Newpro) {
        if (error) res.status(500).send(error.message);

        res.status(201).json(Newpro);
    });
};


exports.producto_update = function (req, res) {

    Prods.findByIdAndUpdate(req.params.id, req.body ,function (error,newPro) {
        if (error) res.status(500).send(error.message);
        res.status(201).json(newPro);
    });

};

