var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductoShema = new Schema({
    id: Number,
    nombre: String,
    precio: Number,
    descripcion: String

});
var Producto = mongoose.model('producto', ProductoShema);


module.exports = Producto;
